from PyQt5.QtWidgets import *
from PyQt5 import QtCore, uic
from PyQt5.QtWidgets import QWidget
import qdarktheme
import sys

class MyGUI(QMainWindow):
    def __init__(self) -> None:
        super(MyGUI, self).__init__()
        uic.loadUi('mygui.ui', self)
        self.show()

        self.pushButton.clicked.connect(self.login)
        self.pushButton_2.clicked.connect(lambda: self.send(self.textEdit.toPlainText()))
        self.actionClose.triggered.connect(exit)
        pass

    def login(self):
        if self.lineEdit.text() == 'jenchieh' and self.lineEdit_2.text() == 'password':
            self.textEdit.setEnabled(True)
            self.pushButton_2.setEnabled(True)
        else:
            message = QMessageBox()
            message.setText('Invalid login')
            message.exec()
        pass

    def send(self, msg):
        message = QMessageBox()
        message.setText(msg)
        message.exec_()
        pass

def main():
    app = QApplication(sys.argv)
    qdarktheme.setup_theme("auto")
    window = MyGUI()
    app.exec_()
    pass

if __name__ == '__main__':
    main()